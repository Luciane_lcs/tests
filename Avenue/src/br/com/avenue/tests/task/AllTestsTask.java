package br.com.avenue.tests.task;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AddTask.class, TaskEmpty.class, TaskSameDescription.class, TaskChareters.class, TaskRemove.class })
public class AllTestsTask {

}
