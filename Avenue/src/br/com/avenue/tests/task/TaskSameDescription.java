package br.com.avenue.tests.task;

import static org.junit.Assert.*;

import org.junit.Test;
import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class TaskSameDescription {

	@Test
	public void test() {

	}

	public class TaskCorrect {
		private WebDriver driver;
		private String baseUrl;
		private boolean acceptNextAlert = true;
		private StringBuffer verificationErrors = new StringBuffer();

		@Before
		public void setUp() throws Exception {
			//Instance Firefox
			driver = new FirefoxDriver();
			baseUrl = "http://qa-test.avenuecode.com/";
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}

		@Test
		public void testTaskCorrect() throws Exception {
			driver.get(baseUrl + "/");
			// Login
			driver.findElement(By.linkText("Sign In")).click();
			driver.findElement(By.id("user_email")).clear();
			driver.findElement(By.id("user_email")).sendKeys("lucianephn@yahoo.com.br");
			driver.findElement(By.id("user_password")).clear();
			driver.findElement(By.id("user_password")).sendKeys("lcs26052016");
			driver.findElement(By.name("commit")).click();
			// Access my task
			driver.findElement(By.linkText("My Tasks")).click();
			driver.findElement(By.id("new_task")).clear();
			driver.findElement(By.id("new_task")).sendKeys("Test 01");
			driver.findElement(By.xpath("//div[2]/span")).click();

			//Validate message
			try {

				assertEquals("Already a task with description, please change description.",
						driver.findElement(By.cssSelector("span.azul2 > strong")).getText());
			} catch (Error e) {
				verificationErrors.append(e.toString());
			}
		}

		@After
		public void tearDown() throws Exception {
			driver.quit();
			String verificationErrorString = verificationErrors.toString();
			if (!"".equals(verificationErrorString)) {
				fail(verificationErrorString);
			}
		}

		private boolean isElementPresent(By by) {
			try {
				driver.findElement(by);
				return true;
			} catch (NoSuchElementException e) {
				return false;
			}
		}

		private boolean isAlertPresent() {
			try {
				driver.switchTo().alert();
				return true;
			} catch (NoAlertPresentException e) {
				return false;
			}
		}

		private String closeAlertAndGetItsText() {
			try {
				Alert alert = driver.switchTo().alert();
				String alertText = alert.getText();
				if (acceptNextAlert) {
					alert.accept();
				} else {
					alert.dismiss();
				}
				return alertText;
			} finally {
				acceptNextAlert = true;
			}
		}
	}

}
