package br.com.avenue.tests.task;

import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TaskChareters {

	public class TaskCorrect {
		private WebDriver driver;
		private String baseUrl;
		private boolean acceptNextAlert = true;
		private StringBuffer verificationErrors = new StringBuffer();

		@Before
		public void setUp() throws Exception {
			// Instance Firefox
			driver = new FirefoxDriver();
			baseUrl = "http://qa-test.avenuecode.com/";
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}

		@Test
		public void testTaskCorrect() throws Exception {
			driver.get(baseUrl + "/");
			// Login
			driver.findElement(By.linkText("Sign In")).click();
			driver.findElement(By.id("user_email")).clear();
			driver.findElement(By.id("user_email")).sendKeys("lucianephn@yahoo.com.br");
			driver.findElement(By.id("user_password")).clear();
			driver.findElement(By.id("user_password")).sendKeys("lcs26052016");
			driver.findElement(By.name("commit")).click();
			// Access My Task
			driver.findElement(By.linkText("My Tasks")).click();
			driver.findElement(By.id("new_task")).clear();
			driver.findElement(By.id("new_task"))
					.sendKeys("111111111111111111111111111111111111111111111111111111111111111111111111"
							+ "1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
							+ "11111111111111111111111111111111111111111www111111111111111111111111111");
			driver.findElement(By.xpath("//div[2]/span")).click();

			// Validate message
			try {

				assertEquals("Task can't insered, because description has more than 250 characters.",
						driver.findElement(By.cssSelector("span.azul2 > strong")).getText());
			} catch (Error e) {
				verificationErrors.append(e.toString());
			}
		}

		@After
		public void tearDown() throws Exception {
			driver.quit();
			String verificationErrorString = verificationErrors.toString();
			if (!"".equals(verificationErrorString)) {
				fail(verificationErrorString);
			}
		}

		private boolean isElementPresent(By by) {
			try {
				driver.findElement(by);
				return true;
			} catch (NoSuchElementException e) {
				return false;
			}
		}

		private boolean isAlertPresent() {
			try {
				driver.switchTo().alert();
				return true;
			} catch (NoAlertPresentException e) {
				return false;
			}
		}

		private String closeAlertAndGetItsText() {
			try {
				Alert alert = driver.switchTo().alert();
				String alertText = alert.getText();
				if (acceptNextAlert) {
					alert.accept();
				} else {
					alert.dismiss();
				}
				return alertText;
			} finally {
				acceptNextAlert = true;
			}
		}
	}

}
