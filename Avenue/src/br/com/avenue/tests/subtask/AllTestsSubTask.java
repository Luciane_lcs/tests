package br.com.avenue.tests.subtask;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import br.com.avenue.tests.task.TaskRemove;

@RunWith(Suite.class)
@SuiteClasses({ AddSubTask.class, SubTaskCharacters.class, SubTaskDateEmpty.class, SubTaskDateInvalid.class,
		SubTaskEmpty.class, SubTaskSameDescription.class, SubTaskRemove.class, TaskRemove.class })
public class AllTestsSubTask {

}
