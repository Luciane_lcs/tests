package br.com.avenue.tests.subtask;

import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;
import static org.testng.Assert.*;
import static org.testng.Assert.fail;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;

public class AddSubTask {
	
	private WebDriver driver;
	private String baseUrl;
	private boolean acceptNextAlert = true;
	private StringBuffer verificationErrors = new StringBuffer();

	@Before
	public void setUp() throws Exception {
		//instance Firefox
		driver = new FirefoxDriver();		
	    baseUrl = "http://qa-test.avenuecode.com";
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
	}

	@Test
	public void testAddSubTask() throws Exception {
		
		driver.get(baseUrl + "/");
		//Login
		driver.findElement(By.linkText("Sign In")).click();
		driver.findElement(By.id("user_email")).clear();
		driver.findElement(By.id("user_email")).sendKeys("lucianephn@yahoo.com.br");
		driver.findElement(By.id("user_password")).clear();
		driver.findElement(By.id("user_password")).sendKeys("lcs26052016");
		driver.findElement(By.name("commit")).click();
		//Access menu My Task
		driver.findElement(By.linkText("My Tasks")).click();
		//Add new Sub Task
		driver.findElement(By.xpath("//tr[8]/td[4]/button")).click();
		driver.findElement(By.id("new_sub_task")).clear();
		driver.findElement(By.id("new_sub_task")).sendKeys("Sub task 1");
		driver.findElement(By.id("dueDate")).clear();
		driver.findElement(By.id("dueDate")).sendKeys("07/07/2016");
		driver.findElement(By.id("add-subtask")).click();
		driver.findElement(By.cssSelector("div.modal-footer.ng-scope > button.btn.btn-primary")).click();

		//Validate message 
		try {

			assertEquals("Sub ask was insered successfuly.",
					driver.findElement(By.cssSelector("span.azul2 > strong")).getText());
		} catch (Error e) {
			verificationErrors.append(e.toString());
		}
	}

	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		driver.quit();
		String verificationErrorString = verificationErrors.toString();
		if (!"".equals(verificationErrorString)) {
			// fail(verificationErrorString);
		}
	}

	private boolean isElementPresent(By by) {
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		}
	}

	private boolean isAlertPresent() {
		try {
			driver.switchTo().alert();
			return true;
		} catch (NoAlertPresentException e) {
			return false;
		}
	}

	private String closeAlertAndGetItsText() {
		try {
			Alert alert = driver.switchTo().alert();
			String alertText = alert.getText();
			if (acceptNextAlert) {
				alert.accept();
			} else {
				alert.dismiss();
			}
			return alertText;
		} finally {
			acceptNextAlert = true;
		}
	}
}
